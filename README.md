# Greek Drupal Community website
Developed with love.

Based on the [Drupal recommended project](https://github.com/drupal/recommended-project)

# Requirements
You can find the Drupal requirements at [Drupal minimum requirements](https://www.drupal.org/docs/system-requirements).

You will also need to have [composer](https://getcomposer.org/doc/00-intro.md) installed in your system.

# Setup
You will need a configured server and an SQL database.

- Copy the `.env.example` file from the project root directory over to the `.env` in the project root directory.
- Override any variable according to your installation.

# installation
From the project root, run
```
$ composer install
```
Then from the project root, run
```
./vendor/bin/drush --yes site:install --existing-config
```

# Contributions
The website is exporting configuration in the `config/sync` directory in the project root. After updating the website,
run
```
./vendor/bin/drush cex
```
to export the configuration. Then commit the configuration.
*WARNING: DO NOT COMMIT ANY SENSITIVE DATA.*
